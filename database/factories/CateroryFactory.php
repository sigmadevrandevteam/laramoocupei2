<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Caterory;
use Faker\Generator as Faker;

$factory->define(Caterory::class, function (Faker $faker) {

    return [
        'title' => $faker->word,
        'photo' => $faker->word,
        'comment' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
