<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Course_highlight;
use Faker\Generator as Faker;

$factory->define(Course_highlight::class, function (Faker $faker) {

    return [
        'id_course' => $faker->randomDigitNotNull,
        'id_category' => $faker->randomDigitNotNull,
        'id_level' => $faker->randomDigitNotNull,
        'id_language' => $faker->randomDigitNotNull,
        'id_type' => $faker->randomDigitNotNull,
        'beginning' => $faker->word,
        'duration' => $faker->word,
        'number_of_lesson' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
