<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User_detail;
use Faker\Generator as Faker;

$factory->define(User_detail::class, function (Faker $faker) {

    return [
        'id_user' => $faker->randomDigitNotNull,
        'full_name' => $faker->word,
        'gender' => $faker->randomDigitNotNull,
        'address' => $faker->word,
        'phone_number' => $faker->randomDigitNotNull,
        'email' => $faker->word,
        'birthday_date' => $faker->word,
        'city' => $faker->word,
        'nationality' => $faker->word,
        'other' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
