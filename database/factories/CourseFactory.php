<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {

    return [
        'title' => $faker->word,
        'photo' => $faker->word,
        'id_user' => $faker->randomDigitNotNull,
        'about_the_course' => $faker->text,
        'topic_include' => $faker->text,
        'view_counts' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
