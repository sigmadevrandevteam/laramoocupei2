<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Student_subscription;
use Faker\Generator as Faker;

$factory->define(Student_subscription::class, function (Faker $faker) {

    return [
        'id_user' => $faker->randomDigitNotNull,
        'id_course' => $faker->randomDigitNotNull,
        'subscription_date' => $faker->word,
        'status' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
