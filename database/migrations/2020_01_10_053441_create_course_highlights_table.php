<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCourseHighlightsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_highlights', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('id_course');
            $table->integer('id_category')->nullable()->default(1);
            $table->integer('id_level')->nullable();
            $table->integer('id_language')->nullable();
            $table->integer('id_type')->nullable();
            $table->date('beginning')->nullable();
            $table->double('duration')->nullable();
            $table->integer('number_of_lesson')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_course')->references('id')->on('courses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('course_highlights');
    }
}
