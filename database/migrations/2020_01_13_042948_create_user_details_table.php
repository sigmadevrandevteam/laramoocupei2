<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_user')->unique();
            $table->string('full_name');
            $table->integer('gender')->nullable()->default(0);
            $table->string('address')->nullable();
            $table->integer('phone_number')->nullable();
            $table->string('email')->nullable();
            $table->date('birthday_date')->nullable();
            $table->string('city')->nullable();
            $table->string('nationality')->nullable();
            $table->text('other')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_details');
    }
}
