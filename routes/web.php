<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::get('/', 'HomeController@index')
    ->name('public.home');

Route::get('/course', 'HomeController@index')
    ->name('public.course.list');


Route::get('/news', 'HomeController@contact')
    ->name('public.news');

Route::get('/contact', 'HomeController@contact')
    ->name('public.contact');

Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');

Auth::routes();

Route::get('/home', 'HomeController@Home');



Route::resource('categories', 'CategoryController');

Route::resource('courses', 'CourseController');



Route::resource('types', 'TypeController');



Route::resource('levels', 'LevelController');

Route::resource('languages', 'LanguageController');

Route::resource('userDetails', 'User_detailController');

Route::resource('studentSubscriptions', 'Student_subscriptionController');

Route::resource('courseHighlights', 'Course_highlightController');

Route::resource('users', 'UserController');

Route::resource('items', 'ItemController');