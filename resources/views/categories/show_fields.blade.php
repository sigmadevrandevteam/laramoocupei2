
<div class="row">

    <div class="col-md-4 col-sm-4">
        <img style=" margin-top: 1.5em" src="/storage/image/category_image/{{ $category->title }}/{{ $category->photo }} ">
    </div>

    <div class="col-md-8 col-sm-8">
        <div class="form-group">

            <p></p>
        </div>

        <div class="course-intro">
            <div class="listing-box clearfix">
                <h5>Category detail</h5>
                <ul class="listing-box-list">
                    <!-- Title Field -->
                    <li>
                        <div class="row gap-10">
                            <div class="col-xs-5 col-sm-6"> {!! Form::label('title', 'Title:') !!}</div>
                            <div class="col-xs-7 col-sm-6 text-right font600"><p>{{ $category->title }}</p></div>
                        </div>
                    </li>

                    <!-- Description Field -->

                    <li>
                        <div class="row gap-10">
                            <div class="col-xs-5 col-sm-6"> {!! Form::label('description', 'Description:') !!}</div>
                            <div class="col-xs-7 col-sm-6 text-right font600">{{ $category->description }}</div>
                        </div>
                    </li>

                    <li>


                        <a href="{{ route('categories.edit', [$category->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}

                    </li>
                </ul>
            </div>
        </div>
    </div>

</div>


