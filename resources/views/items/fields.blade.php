<!-- Id Course Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_course', 'Id Course:') !!}
    {!! Form::number('id_course', null, ['class' => 'form-control']) !!}
</div>

<!-- File Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file', 'File:') !!}
    {!! Form::file('file') !!}
</div>
<div class="clearfix"></div>

<!-- File Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_type', 'File Type:') !!}
    {!! Form::select('file_type', ['1' => 'video', '2' => 'audio', '3' => 'pdf', '4' => 'other'], null, ['class' => 'form-control']) !!}
</div>

<!-- Label Field -->
<div class="form-group col-sm-6">
    {!! Form::label('label', 'Label:') !!}
    {!! Form::text('label', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('items.index') }}" class="btn btn-secondary">Cancel</a>
</div>
