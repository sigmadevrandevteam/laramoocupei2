<!-- Id Course Field -->
<div class="form-group">
    {!! Form::label('id_course', 'Id Course:') !!}
    <p>{{ $item->id_course }}</p>
</div>

<!-- File Field -->
<div class="form-group">
    {!! Form::label('file', 'File:') !!}
    <p>{{ $item->file }}</p>
</div>

<!-- File Type Field -->
<div class="form-group">
    {!! Form::label('file_type', 'File Type:') !!}
    <p>{{ $item->file_type }}</p>
</div>

<!-- Label Field -->
<div class="form-group">
    {!! Form::label('label', 'Label:') !!}
    <p>{{ $item->label }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $item->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $item->updated_at }}</p>
</div>

