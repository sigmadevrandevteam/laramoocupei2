xtends('layouts.frontOffice.course.app')

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('/public/fronts/styles/courses_styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/public/fronts/styles/courses_responsive.css') }}">
@stop

@section('content')
	<!-- Home -->
    @foreach($course_datas['course_data'] as $key => $cours)
	<div class="home">
		<div class="home_background_container prlx_parent">
			<div class="home_background prlx" style="background-image:url({{ asset('public/fronts/images/courses_background.jpg') }})"></div>
        </div>
		<div class="home_content">
			<h1>Courses</h1>
		</div>
	</div>

	<!-- Popular -->

	<div class="popular page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>{{$cours->title}}</h1>
					</div>
				</div>
            </div>
            <div class="row">
				<div class="col-12">
					<div class="w-100">
						<p>{{$cours->short_desc}}</p>
					</div>
				</div>
			</div>

			<div class="row course_boxes">

				<!-- Popular Course Item -->
				<div class="col-lg-3">
					<div class="w-100">
						<div class="course_author_name">{{$cours->name}} {{$cours->prename}}, <span>Proffesseur responsable</span></div>
					</div>
				</div>
				<div class="col-lg-6 course_box">
					@foreach($course_datas['assets_data'] as $key => $asset)
						@if($asset->assets_type_id == "1")
					<div class="card">
						<div class="">
						<video class="card-img-top" controls src="{{asset('/public/')}}/{{$asset->assets_fileurl}}" alt="https://unsplash.com/@kellybrito"> </video>
						</div>
						<div class="card-body text-center">
							<div class="card-text pt-4">Du {{$cours->date_debut}} au {{$cours->date_fin}}</div>
						</div>
						<div class="price_box d-flex flex-row align-items-center">
							<div class="course_author_image">
								<img src="{{ asset('public/fronts/images/author.jpg') }}" alt="https://unsplash.com/@mehdizadeh">
							</div>
						</div>
					</div>
					<div class="w-100">
						<p>{{strip_tags($cours->long_desc)}}</p>
					</div>
						@endif
					@endforeach
                </div>
				<div class="col-lg-3">
					<div class="w-100 text-center">
						<h3 class="text-black">Support de cours :</h3>
						@foreach($course_datas['assets_data'] as $key => $asset)
							@if($asset->assets_type_id != "1")
								<div class="w-100 pb-4">
								@if($asset->type_id == "2")
									<div class="w-100">{{$asset->assets_title}} :</div>
									<p>{{$asset->assets_short_description}}</p>
									<a href="{{asset('/public/')}}/{{$asset->assets_fileurl}}"><button class="btn btn-secondary">{{$asset->assets_filename}}</button></a>
								@endif
								</div>
							@endif
						@endforeach
					</div>
				</div>
			<?php /* ?>
				Popular Course Item
				<div class="col-lg-4 course_box">
					<div class="card">
						<img class="card-img-top" src="{{ asset('public/fronts/images/course_2.jpg') }}" alt="https://unsplash.com/@cikstefan">
						<div class="card-body text-center">
							<div class="card-title"><a href="courses.html">Beginners guide to HTML</a></div>
							<div class="card-text">Adobe Guide, Layes, Smart Objects etc...</div>
						</div>
						<div class="price_box d-flex flex-row align-items-center">
							<div class="course_author_image">
								<img src="{{ asset('public/fronts/images/author.jpg') }}" alt="https://unsplash.com/@mehdizadeh">
							</div>
							<div class="course_author_name">Michael Smith, <span>Author</span></div>
							<div class="course_price d-flex flex-column align-items-center justify-content-center"><span>$29</span></div>
						</div>
					</div>
				</div>

				<!-- Popular Course Item -->
				<div class="col-lg-4 course_box">
					<div class="card">
						<img class="card-img-top" src="{{ asset('public/fronts/images/course_3.jpg') }}" alt="https://unsplash.com/@dsmacinnes">
						<div class="card-body text-center">
							<div class="card-title"><a href="courses.html">Advanced Photoshop</a></div>
							<div class="card-text">Adobe Guide, Layes, Smart Objects etc...</div>
						</div>
						<div class="price_box d-flex flex-row align-items-center">
							<div class="course_author_image">
								<img src="{{ asset('public/fronts/images/author.jpg') }}" alt="https://unsplash.com/@mehdizadeh">
							</div>
							<div class="course_author_name">Michael Smith, <span>Author</span></div>
							<div class="course_price d-flex flex-column align-items-center justify-content-center"><span>$29</span></div>
						</div>
					</div>
				</div>

				<!-- Popular Course Item -->
				<div class="col-lg-4 course_box">
					<div class="card">
						<img class="card-img-top" src="{{ asset('public/fronts/images/course_4.jpg') }}" alt="https://unsplash.com/@kellitungay">
						<div class="card-body text-center">
							<div class="card-title"><a href="courses.html">A complete guide to design</a></div>
							<div class="card-text">Adobe Guide, Layes, Smart Objects etc...</div>
						</div>
						<div class="price_box d-flex flex-row align-items-center">
							<div class="course_author_image">
								<img src="{{ asset('public/fronts/images/author.jpg') }}" alt="https://unsplash.com/@mehdizadeh">
							</div>
							<div class="course_author_name">Michael Smith, <span>Author</span></div>
							<div class="course_price d-flex flex-column align-items-center justify-content-center"><span>$29</span></div>
						</div>
					</div>
				</div>

				<!-- Popular Course Item -->
				<div class="col-lg-4 course_box">
					<div class="card">
						<img class="card-img-top" src="{{ asset('public/fronts/images/course_5.jpg') }}" alt="https://unsplash.com/@claybanks1989">
						<div class="card-body text-center">
							<div class="card-title"><a href="courses.html">Beginners guide to HTML</a></div>
							<div class="card-text">Adobe Guide, Layes, Smart Objects etc...</div>
						</div>
						<div class="price_box d-flex flex-row align-items-center">
							<div class="course_author_image">
								<img src="{{ asset('public/fronts/images/author.jpg') }}" alt="https://unsplash.com/@mehdizadeh">
							</div>
							<div class="course_author_name">Michael Smith, <span>Author</span></div>
							<div class="course_price d-flex flex-column align-items-center justify-content-center"><span>$29</span></div>
						</div>
					</div>
				</div>

				<!-- Popular Course Item -->
				<div class="col-lg-4 course_box">
					<div class="card">
						<img class="card-img-top" src="{{ asset('public/fronts/images/course_6.jpg') }}" alt="https://unsplash.com/@element5digital">
						<div class="card-body text-center">
							<div class="card-title"><a href="courses.html">Advanced Photoshop</a></div>
							<div class="card-text">Adobe Guide, Layes, Smart Objects etc...</div>
						</div>
						<div class="price_box d-flex flex-row align-items-center">
							<div class="course_author_image">
								<img src="{{ asset('public/fronts/images/author.jpg') }}" alt="https://unsplash.com/@mehdizadeh">
							</div>
							<div class="course_author_name">Michael Smith, <span>Author</span></div>
							<div class="course_price d-flex flex-column align-items-center justify-content-center"><span>$29</span></div>
						</div>
					</div>
				</div>

				<!-- Popular Course Item -->
				<div class="col-lg-4 course_box">
					<div class="card">
						<img class="card-img-top" src="{{ asset('public/fronts/images/course_7.jpg') }}" alt="https://unsplash.com/@gaellemm">
						<div class="card-body text-center">
							<div class="card-title"><a href="courses.html">A complete guide to design</a></div>
							<div class="card-text">Adobe Guide, Layes, Smart Objects etc...</div>
						</div>
						<div class="price_box d-flex flex-row align-items-center">
							<div class="course_author_image">
								<img src="{{ asset('public/fronts/images/author.jpg') }}" alt="https://unsplash.com/@mehdizadeh">
							</div>
							<div class="course_author_name">Michael Smith, <span>Author</span></div>
							<div class="course_price d-flex flex-column align-items-center justify-content-center"><span>$29</span></div>
						</div>
					</div>
				</div>

				<!-- Popular Course Item -->
				<div class="col-lg-4 course_box">
					<div class="card">
						<img class="card-img-top" src="{{ asset('public/fronts/images/course_8.jpg') }}" alt="https://unsplash.com/@juanmramosjr">
						<div class="card-body text-center">
							<div class="card-title"><a href="courses.html">Beginners guide to HTML</a></div>
							<div class="card-text">Adobe Guide, Layes, Smart Objects etc...</div>
						</div>
						<div class="price_box d-flex flex-row align-items-center">
							<div class="course_author_image">
								<img src="{{ asset('public/fronts/images/author.jpg') }}" alt="https://unsplash.com/@mehdizadeh">
							</div>
							<div class="course_author_name">Michael Smith, <span>Author</span></div>
							<div class="course_price d-flex flex-column align-items-center justify-content-center"><span>$29</span></div>
						</div>
					</div>
				</div>

				<!-- Popular Course Item -->
				<div class="col-lg-4 course_box">
					<div class="card">
						<img class="card-img-top" src="{{ asset('public/fronts/images/course_9.jpg') }}" alt="https://unsplash.com/@kimberlyfarmer">
						<div class="card-body text-center">
							<div class="card-title"><a href="courses.html">Advanced Photoshop</a></div>
							<div class="card-text">Adobe Guide, Layes, Smart Objects etc...</div>
						</div>
						<div class="price_box d-flex flex-row align-items-center">
							<div class="course_author_image">
								<img src="images/author.jpg" alt="https://unsplash.com/@mehdizadeh">
							</div>
							<div class="course_author_name">Michael Smith, <span>Author</span></div>
							<div class="course_price d-flex flex-column align-items-center justify-content-center"><span>$29</span></div>
						</div>
					</div>
				</div>
			<?php */ ?>
			</div>
		</div>
    </div>
    @endforeach
@endsection