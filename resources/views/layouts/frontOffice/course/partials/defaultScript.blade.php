<script src="{{ asset('public/fronts/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('public/fronts/styles/bootstrap4/popper.js') }}"></script>
<script src="{{ asset('public/fronts/styles/bootstrap4/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/fronts/plugins/greensock/TweenMax.min.js') }}"></script>
<script src="{{ asset('public/fronts/plugins/greensock/TimelineMax.min.js') }}"></script>
<script src="{{ asset('public/fronts/plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
<script src="{{ asset('public/fronts/plugins/greensock/animation.gsap.min.js') }}"></script>
<script src="{{ asset('public/fronts/plugins/greensock/ScrollToPlugin.min.js') }}"></script>
<script src="{{ asset('public/fronts/plugins/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
<script src="{{ asset('public/fronts/plugins/scrollTo/jquery.scrollTo.min.js') }}"></script>
<script src="{{ asset('public/fronts/plugins/easing/easing.js') }}"></script>
<script src="{{ asset('public/fronts/js/custom.js') }}"></script>

