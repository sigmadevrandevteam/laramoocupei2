

            {{--teacher section--}}

@if( Auth::user()->Role_type == 4 )

    <li class="nav-item {{ Request::is('categories*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('categories.index') }}">
            <i class="fa fa-coins mr-2"></i>
            <span>Categories</span>
        </a>
    </li>
    <li class="nav-item {{ Request::is('courses*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('courses.index') }}">
            <i class="fa fa-swatchbook mr-2"></i>
            <span> My courses </span>
        </a>
    </li>


    <li class="nav-item ">
        <a class="nav-link" href="#">
            <i class="fa fa-swatchbook mr-2"></i>
            <span> My Students </span>
        </a>
    </li>

            {{--student section--}}
@elseif( Auth::user()->Role_type == 3  )

    <li class="nav-item {{ Request::is('categories*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('categories.index') }}">
            <i class="fa fa-coins mr-2"></i>
            <span>Categories</span>
        </a>
    </li>
    <li class="nav-item {{ Request::is('courses*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('courses.index') }}">
            <i class="fa fa-swatchbook mr-2"></i>
            <span> My courses </span>
        </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="#">
            <i class="fa fa-swatchbook mr-2"></i>
            <span> My  Teachers </span>
        </a>
    </li>
            {{--admin section--}}
@elseif( Auth::user()->Role_type < 3  )


<li class="nav-item {{ Request::is('categories*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('categories.index') }}">
        <i class="fa fa-coins mr-2"></i>
        <span>Categories</span>
    </a>
</li>
<li class="nav-item {{ Request::is('courses*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('courses.index') }}">
        <i class="fa fa-swatchbook mr-2"></i>
        <span>Courses</span>
    </a>
</li>
<li class="nav-item {{ Request::is('courseHighlights*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('courseHighlights.index') }}">
        <i class="fa fa-info mr-3"></i>
        <span>Course Highlights</span>
    </a>
</li>
<li class="nav-item {{ Request::is('items*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('items.index') }}">
        <i class="fa fa-folder-plus mr-2"></i>
        <span>Items</span>
    </a>
</li>
<li class="nav-item {{ Request::is('types*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('types.index') }}">
        <i class="fa fa-clipboard-list mr-2"></i>
        <span>Types</span>
    </a>
</li>
<li class="nav-item {{ Request::is('levels*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('levels.index') }}">
        <i class="fa fa-align-justify mr-2"></i>
        <span>Levels</span>
    </a>
</li>
<li class="nav-item {{ Request::is('languages*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('languages.index') }}">
        <i class="fa fa-language mr-2"></i>
        <span>Languages</span>
    </a>
<li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('users.index') }}">
        <i class="fa fa-address-card"></i>
        <span>Users</span>
    </a>
</li>
</li>
<li class="nav-item {{ Request::is('userDetails*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('userDetails.index') }}">
        <i class="fas fa-address-card mr-2"></i>
        <span>User Details</span>
    </a>
</li>

<li class="nav-item {{ Request::is('studentSubscriptions*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('studentSubscriptions.index') }}">
        <i class="fa fa-copy mr-3"></i>
        <span>Student Subscriptions</span>
    </a>
</li>

    @endif
