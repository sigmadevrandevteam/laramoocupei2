<div class="table-responsive-sm">
    <table class="table table-striped" id="courseHighlights-table">
        <thead>
            <th>Course</th>
        <th>Category</th>
        <th>Level</th>
        <th>Language</th>
        <th>Type</th>
        <th>Beginning</th>
        <th>Duration</th>
        <th>Number Of Lesson</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($courseHighlights as $courseHighlight)
            <tr>
                <td>{{ $courseHighlight->id_course }}</td>
            <td>{{ $courseHighlight->id_category }}</td>
            <td>{{ $courseHighlight->id_level }}</td>
            <td>{{ $courseHighlight->id_language }}</td>
            <td>{{ $courseHighlight->id_type }}</td>
            <td>{{ $courseHighlight->beginning }}</td>
            <td>{{ $courseHighlight->duration }}</td>
            <td>{{ $courseHighlight->number_of_lesson }}</td>
                <td>
                    {!! Form::open(['route' => ['courseHighlights.destroy', $courseHighlight->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('courseHighlights.show', [$courseHighlight->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('courseHighlights.edit', [$courseHighlight->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>