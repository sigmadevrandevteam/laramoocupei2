

<div class="course-intro">
    <div class="listing-box clearfix">
        <h5>Course Highlight for : {{$courseHighlight->id_course}}</h5>
        <ul class="listing-box-list">

            <li>
                <div class="row gap-10">
                    <div class="col-xs-5 col-sm-6"><i class="fa fa-info mr-5"></i> {!! Form::label('id_category', 'Category:') !!}</div>
                    <div class="col-xs-7 col-sm-6 text-right font600">{{ $courseHighlight->id_category }}</div>
                </div>
            </li>
            <li>
                <div class="row gap-10">
                    <div class="col-xs-5 col-sm-6"><i class="fa fa-bars mr-5"></i> {!! Form::label('id_level', 'Level:') !!}</div>
                    @if($courseHighlight->id_level = 1)
                    <div class="col-xs-7 col-sm-6 text-right font600">Premiere annee</div>
                        @elseif($courseHighlight->id_level = 2)
                        <div class="col-xs-7 col-sm-6 text-right font600">Deuxieme annee</div>
                    @elseif($courseHighlight->id_level = 3)
                        <div class="col-xs-7 col-sm-6 text-right font600">Troisieme annee</div>
                    @elseif($courseHighlight->id_level = 4)
                        <div class="col-xs-7 col-sm-6 text-right font600">Master One</div>
                    @elseif($courseHighlight->id_level = 5)
                        <div class="col-xs-7 col-sm-6 text-right font600">Master Two</div>
                    @endif
                </div>
            </li>
            <li>
                <div class="row gap-10">
                    <div class="col-xs-5 col-sm-6"><i class="fa fa-language"></i> Language :</div>
                   @if( $courseHighlight->id_language = 1)
                    <div class="col-xs-7 col-sm-6 text-right font600">English</div>
                    @elseif($courseHighlight->id_language = 2)
                        <div class="col-xs-7 col-sm-6 text-right font600">French</div>
                    @endif
                </div>
            </li>
            <li>
                <div class="row gap-10">
                    <div class="col-xs-5 col-sm-6"><i class="fa fa-clock mr-5"></i> Duration</div>
                    <div class="col-xs-7 col-sm-6 text-right font600">{{ $courseHighlight->duration }} Hours</div>
                </div>
            </li>
            <li>
                <div class="row gap-10">
                    <div class="col-xs-5 col-sm-5"><i class="fa fa-calendar mr-5"></i> {!! Form::label('beginning', 'Beginning:') !!}</div>
                    <div class="col-xs-7 col-sm-7 text-right font600">{{ $courseHighlight->beginning }}</div>
                </div>
            </li>
            <li>
                <div class="row gap-10">
                    <div class="col-xs-5 col-sm-5"><i class="fa fa-pen-nib mr-5"></i> {!! Form::label('number_of_lesson', 'Lesson:') !!}</div>
                    <div class="col-xs-7 col-sm-7 text-right font600"> {{ $courseHighlight->number_of_lesson }} lessons</div>
                </div>
            </li>
            <li>
                <div class="row gap-10">
                    <div class="col-xs-5 col-sm-5"><i class="fa fa-file-video mr-5"></i> Type</div>
                    @if( $courseHighlight->id_type = 1)
                    <div class="col-xs-7 col-sm-7 text-right font600"> Video online</div>
                        @elseif($courseHighlight->id_type = 3)
                        <div class="col-xs-7 col-sm-7 text-right font600"> Presence Learning</div>
                    @elseif($courseHighlight->id_type = 2)
                        <div class="col-xs-7 col-sm-7 text-right font600"> Distance Learning</div>
                        @endif
                </div>
            </li>

            <li>
                <div class="row gap-10">
                    <div class="col-xs-5 col-sm-5">
                    <a href="{{ route('courseHighlights.show', [$courseHighlight->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                    </div>
                    <div class="col-xs-5 col-sm-5">
                    <a href="{{ route('courseHighlights.edit', [$courseHighlight->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                    </div>
                </div>

            </li>
        </ul>
    </div>
</div>



{{--
<!-- Id Course Field -->
<div class="form-group">
    {!! Form::label('id_course', 'Course:') !!}
    <p>{{ $courseHighlight->id_course }}</p>
</div>

<!-- Id Category Field -->
<div class="form-group">
    {!! Form::label('id_category', 'Category:') !!}
    <p>{{ $courseHighlight->id_category }}</p>
</div>

<!-- Id Level Field -->
<div class="form-group">
    {!! Form::label('id_level', 'Level:') !!}
    <p>{{ $courseHighlight->id_level }}</p>
</div>

<!-- Id Language Field -->
<div class="form-group">
    {!! Form::label('id_language', 'Language:') !!}
    <p>{{ $courseHighlight->id_language }}</p>
</div>

<!-- Id Type Field -->
<div class="form-group">
    {!! Form::label('id_type', 'Type:') !!}
    <p>{{ $courseHighlight->id_type }}</p>
</div>

<!-- Beginning Field -->
<div class="form-group">
    {!! Form::label('beginning', 'Beginning:') !!}
    <p>{{ $courseHighlight->beginning }}</p>
</div>

<!-- Duration Field -->
<div class="form-group">
    {!! Form::label('duration', 'Duration:') !!}
    <p>{{ $courseHighlight->duration }}</p>
</div>

<!-- Number Of Lesson Field -->
<div class="form-group">
    {!! Form::label('number_of_lesson', 'Number Of Lesson:') !!}
    <p>{{ $courseHighlight->number_of_lesson }}</p>
</div>



--}}
