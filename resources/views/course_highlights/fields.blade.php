<!-- Id Course Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_course', 'Course:') !!}
    {!! Form::number('id_course', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_category', 'Category:') !!}
    {!! Form::number('id_category', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Level Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_level', 'Level:') !!}
    {!! Form::select('id_level', ['1' => 'Premiere Annee', '2' => 'Deuxieme Annee', '3' => 'Troisieme annee', '4' => 'Master 1', '5' => 'master 2'], null, ['class' => 'form-control']) !!}
</div>

<!-- Id Language Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_language', 'Language:') !!}
    {!! Form::select('id_language', ['1' => 'English', '2' => 'French'], null, ['class' => 'form-control']) !!}
</div>

<!-- Id Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_type', 'Type:') !!}
    {!! Form::select('id_type', ['1' => 'Video online', '2' => 'Distance learning', '3' => 'Presence Learning'], null, ['class' => 'form-control']) !!}
</div>

<!-- Beginning Field -->
<div class="form-group col-sm-6">
    {!! Form::label('beginning', 'Beginning:') !!}
    {!! Form::date('beginning', null, ['class' => 'form-control','id'=>'beginning']) !!}
</div>

@section('scripts')
   <script type="text/javascript">
           $('#beginning').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endsection

<!-- Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duration', 'Duration:') !!}
    {!! Form::number('duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Number Of Lesson Field -->
<div class="form-group col-sm-6">
    {!! Form::label('number_of_lesson', 'Number Of Lesson:') !!}
    {!! Form::number('number_of_lesson', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('courseHighlights.index') }}" class="btn btn-secondary">Cancel</a>
</div>
