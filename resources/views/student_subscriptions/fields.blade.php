<!-- Id User Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_user', 'Id User:') !!}
    {!! Form::number('id_user', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Course Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_course', 'Id Course:') !!}
    {!! Form::number('id_course', null, ['class' => 'form-control']) !!}
</div>

<!-- Subscription Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subscription_date', 'Subscription Date:') !!}
    {!! Form::text('subscription_date', null, ['class' => 'form-control','id'=>'subscription_date']) !!}
</div>

@section('scripts')
   <script type="text/javascript">
           $('#subscription_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endsection

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::number('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('studentSubscriptions.index') }}" class="btn btn-secondary">Cancel</a>
</div>
