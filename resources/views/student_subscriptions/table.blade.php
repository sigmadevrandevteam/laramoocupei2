<div class="table-responsive-sm">
    <table class="table table-striped" id="studentSubscriptions-table">
        <thead>
            <th>Id User</th>
        <th>Id Course</th>
        <th>Subscription Date</th>
        <th>Status</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($studentSubscriptions as $studentSubscription)
            <tr>
                <td>{{ $studentSubscription->id_user }}</td>
            <td>{{ $studentSubscription->id_course }}</td>
            <td>{{ $studentSubscription->subscription_date }}</td>
            <td>{{ $studentSubscription->status }}</td>
                <td>
                    {!! Form::open(['route' => ['studentSubscriptions.destroy', $studentSubscription->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('studentSubscriptions.show', [$studentSubscription->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('studentSubscriptions.edit', [$studentSubscription->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>