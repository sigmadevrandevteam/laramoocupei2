<!-- Id User Field -->
<div class="form-group">
    {!! Form::label('id_user', 'Id User:') !!}
    <p>{{ $studentSubscription->id_user }}</p>
</div>

<!-- Id Course Field -->
<div class="form-group">
    {!! Form::label('id_course', 'Id Course:') !!}
    <p>{{ $studentSubscription->id_course }}</p>
</div>

<!-- Subscription Date Field -->
<div class="form-group">
    {!! Form::label('subscription_date', 'Subscription Date:') !!}
    <p>{{ $studentSubscription->subscription_date }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $studentSubscription->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $studentSubscription->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $studentSubscription->updated_at }}</p>
</div>

