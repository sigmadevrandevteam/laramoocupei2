<div class="table-responsive-sm">
    <table class="table table-striped" id="userDetails-table">
        <thead>
            <th>Id User</th>
        <th>Full Name</th>
        <th>Gender</th>
        <th>Address</th>
        <th>Phone Number</th>
        <th>Email</th>
        <th>Birthday Date</th>
        <th>City</th>
        <th>Nationality</th>
        <th>Other</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($userDetails as $userDetail)
            <tr>
                <td>{{ $userDetail->id_user }}</td>
            <td>{{ $userDetail->full_name }}</td>
            <td>{{ $userDetail->gender }}</td>
            <td>{{ $userDetail->address }}</td>
            <td>{{ $userDetail->phone_number }}</td>
            <td>{{ $userDetail->email }}</td>
            <td>{{ $userDetail->birthday_date }}</td>
            <td>{{ $userDetail->city }}</td>
            <td>{{ $userDetail->nationality }}</td>
            <td>{{ $userDetail->other }}</td>
                <td>
                    {!! Form::open(['route' => ['userDetails.destroy', $userDetail->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('userDetails.show', [$userDetail->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('userDetails.edit', [$userDetail->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>