<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Photo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::file('photo') !!}
</div>
<div class="clearfix"></div>

<!-- Id User Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_user', 'Id User:') !!}
    {!! Form::number('id_user', null, ['class' => 'form-control']) !!}
</div>

<!-- About The Course Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('about_the_course', 'About The Course:') !!}
    {!! Form::textarea('about_the_course', null, ['class' => 'form-control']) !!}
</div>

<!-- Topic Include Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('topic_include', 'Topic Include:') !!}
    {!! Form::textarea('topic_include', null, ['class' => 'form-control']) !!}
</div>

<!-- View Counts Field -->
<div class="form-group col-sm-6">
    {!! Form::label('view_counts', 'View Counts:') !!}
    {!! Form::number('view_counts', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('courses.index') }}" class="btn btn-secondary">Cancel</a>
</div>
