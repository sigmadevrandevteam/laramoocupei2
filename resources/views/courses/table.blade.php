<div class="table-responsive-sm">
    <table class="table table-striped" id="courses-table">
        <thead>
            <th>Title</th>
        <th>Photo</th>
        <th>Id User</th>
        <th>About The Course</th>
        <th>Topic Include</th>
        <th>View Counts</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($courses as $course)
            <tr>
                <td>{{ $course->title }}</td>
            <td>{{ $course->photo }}</td>
            <td>{{ $course->id_user }}</td>
            <td>{{ $course->about_the_course }}</td>
            <td>{{ $course->topic_include }}</td>
            <td>{{ $course->view_counts }}</td>
                <td>
                    {!! Form::open(['route' => ['courses.destroy', $course->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('courses.show', [$course->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('courses.edit', [$course->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>