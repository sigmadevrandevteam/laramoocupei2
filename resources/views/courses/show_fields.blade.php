<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $course->title }}</p>
</div>

<!-- Photo Field -->
<div class="form-group">
    {!! Form::label('photo', 'Photo:') !!}
    <p>{{ $course->photo }}</p>
</div>

<!-- Id User Field -->
<div class="form-group">
    {!! Form::label('id_user', 'Id User:') !!}
    <p>{{ $course->id_user }}</p>
</div>

<!-- About The Course Field -->
<div class="form-group">
    {!! Form::label('about_the_course', 'About The Course:') !!}
    <p>{{ $course->about_the_course }}</p>
</div>

<!-- Topic Include Field -->
<div class="form-group">
    {!! Form::label('topic_include', 'Topic Include:') !!}
    <p>{{ $course->topic_include }}</p>
</div>

<!-- View Counts Field -->
<div class="form-group">
    {!! Form::label('view_counts', 'View Counts:') !!}
    <p>{{ $course->view_counts }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $course->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $course->updated_at }}</p>
</div>

