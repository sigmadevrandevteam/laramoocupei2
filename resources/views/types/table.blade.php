<div class="table-responsive-sm">
    <table class="table table-striped" id="types-table">
        <thead>
            <th>Title</th>
        <th>Photo</th>
        <th>Description</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($types as $type)
            <tr>
                <td>{{ $type->title }}</td>
            <td>{{ $type->photo }}</td>
            <td>{{ $type->description }}</td>
                <td>
                    {!! Form::open(['route' => ['types.destroy', $type->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('types.show', [$type->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('types.edit', [$type->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>