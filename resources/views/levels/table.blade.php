<div class="table-responsive-sm">
    <table class="table table-striped" id="levels-table">
        <thead>
            <th>Title</th>
        <th>Photo</th>
        <th>Description</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($levels as $level)
            <tr>
                <td>{{ $level->title }}</td>
            <td>{{ $level->photo }}</td>
            <td>{{ $level->description }}</td>
                <td>
                    {!! Form::open(['route' => ['levels.destroy', $level->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('levels.show', [$level->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('levels.edit', [$level->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>