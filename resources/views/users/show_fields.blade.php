<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $user->name }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $user->email }}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{{ $user->password }}</p>
</div>

<!-- Role Type Field -->
<div class="form-group">
    {!! Form::label('Role', 'Role type:') !!}

    @if($user->Role_type == 1)
    <p>Admin</p>
    @elseif($user->Role_type == 2 )
        <p> Super Admin</p>
    @elseif($user->Role_type == 2 )
        <p> Student</p>
    @elseif($user->Role_type == 4 )
        <p> Teacher</p>
        @endif
</div>


