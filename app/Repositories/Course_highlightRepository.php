<?php

namespace App\Repositories;

use App\Models\Course_highlight;
use App\Repositories\BaseRepository;

/**
 * Class Course_highlightRepository
 * @package App\Repositories
 * @version January 10, 2020, 5:34 am UTC
*/

class Course_highlightRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_course',
        'id_category',
        'id_level',
        'id_language',
        'id_type',
        'beginning',
        'duration',
        'number_of_lesson'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Course_highlight::class;
    }
}
