<?php

namespace App\Repositories;

use App\Models\User_detail;
use App\Repositories\BaseRepository;

/**
 * Class User_detailRepository
 * @package App\Repositories
 * @version January 8, 2020, 5:15 pm UTC
*/

class User_detailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_user',
        'full_name',
        'gender',
        'address',
        'phone_number',
        'email',
        'birthday_date',
        'city',
        'nationality',
        'other'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User_detail::class;
    }
}
