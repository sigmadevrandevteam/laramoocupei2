<?php

namespace App\Repositories;

use App\Models\Student_subscription;
use App\Repositories\BaseRepository;

/**
 * Class Student_subscriptionRepository
 * @package App\Repositories
 * @version January 8, 2020, 5:21 pm UTC
*/

class Student_subscriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_user',
        'id_course',
        'subscription_date',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Student_subscription::class;
    }
}
