<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCourse_highlightRequest;
use App\Http\Requests\UpdateCourse_highlightRequest;
use App\Repositories\Course_highlightRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class Course_highlightController extends AppBaseController
{
    /** @var  Course_highlightRepository */
    private $courseHighlightRepository;

    public function __construct(Course_highlightRepository $courseHighlightRepo)
    {
        $this->courseHighlightRepository = $courseHighlightRepo;
    }

    /**
     * Display a listing of the Course_highlight.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $courseHighlights = $this->courseHighlightRepository->all();

        return view('course_highlights.index')
            ->with('courseHighlights', $courseHighlights);
    }

    /**
     * Show the form for creating a new Course_highlight.
     *
     * @return Response
     */
    public function create()
    {
        return view('course_highlights.create');
    }

    /**
     * Store a newly created Course_highlight in storage.
     *
     * @param CreateCourse_highlightRequest $request
     *
     * @return Response
     */
    public function store(CreateCourse_highlightRequest $request)
    {
        $input = $request->all();

        $courseHighlight = $this->courseHighlightRepository->create($input);

        Flash::success('Course Highlight saved successfully.');

        return redirect(route('courseHighlights.index'));
    }

    /**
     * Display the specified Course_highlight.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $courseHighlight = $this->courseHighlightRepository->find($id);

        if (empty($courseHighlight)) {
            Flash::error('Course Highlight not found');

            return redirect(route('courseHighlights.index'));
        }

        return view('course_highlights.show')->with('courseHighlight', $courseHighlight);
    }

    /**
     * Show the form for editing the specified Course_highlight.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $courseHighlight = $this->courseHighlightRepository->find($id);

        if (empty($courseHighlight)) {
            Flash::error('Course Highlight not found');

            return redirect(route('courseHighlights.index'));
        }

        return view('course_highlights.edit')->with('courseHighlight', $courseHighlight);
    }

    /**
     * Update the specified Course_highlight in storage.
     *
     * @param int $id
     * @param UpdateCourse_highlightRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCourse_highlightRequest $request)
    {
        $courseHighlight = $this->courseHighlightRepository->find($id);

        if (empty($courseHighlight)) {
            Flash::error('Course Highlight not found');

            return redirect(route('courseHighlights.index'));
        }

        $courseHighlight = $this->courseHighlightRepository->update($request->all(), $id);

        Flash::success('Course Highlight updated successfully.');

        return redirect(route('courseHighlights.index'));
    }

    /**
     * Remove the specified Course_highlight from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $courseHighlight = $this->courseHighlightRepository->find($id);

        if (empty($courseHighlight)) {
            Flash::error('Course Highlight not found');

            return redirect(route('courseHighlights.index'));
        }

        $this->courseHighlightRepository->delete($id);

        Flash::success('Course Highlight deleted successfully.');

        return redirect(route('courseHighlights.index'));
    }
}
