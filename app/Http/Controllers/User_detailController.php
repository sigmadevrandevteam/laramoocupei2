<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUser_detailRequest;
use App\Http\Requests\UpdateUser_detailRequest;
use App\Repositories\User_detailRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class User_detailController extends AppBaseController
{
    /** @var  User_detailRepository */
    private $userDetailRepository;

    public function __construct(User_detailRepository $userDetailRepo)
    {
        $this->userDetailRepository = $userDetailRepo;
    }

    /**
     * Display a listing of the User_detail.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $userDetails = $this->userDetailRepository->all();

        return view('user_details.index')
            ->with('userDetails', $userDetails);
    }

    /**
     * Show the form for creating a new User_detail.
     *
     * @return Response
     */
    public function create()
    {
        return view('user_details.create');
    }

    /**
     * Store a newly created User_detail in storage.
     *
     * @param CreateUser_detailRequest $request
     *
     * @return Response
     */
    public function store(CreateUser_detailRequest $request)
    {
        $input = $request->all();

        $userDetail = $this->userDetailRepository->create($input);

        Flash::success('User Detail saved successfully.');

        return redirect(route('userDetails.index'));
    }

    /**
     * Display the specified User_detail.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userDetail = $this->userDetailRepository->find($id);

        if (empty($userDetail)) {
            Flash::error('User Detail not found');

            return redirect(route('userDetails.index'));
        }

        return view('user_details.show')->with('userDetail', $userDetail);
    }

    /**
     * Show the form for editing the specified User_detail.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userDetail = $this->userDetailRepository->find($id);

        if (empty($userDetail)) {
            Flash::error('User Detail not found');

            return redirect(route('userDetails.index'));
        }

        return view('user_details.edit')->with('userDetail', $userDetail);
    }

    /**
     * Update the specified User_detail in storage.
     *
     * @param int $id
     * @param UpdateUser_detailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUser_detailRequest $request)
    {
        $userDetail = $this->userDetailRepository->find($id);

        if (empty($userDetail)) {
            Flash::error('User Detail not found');

            return redirect(route('userDetails.index'));
        }

        $userDetail = $this->userDetailRepository->update($request->all(), $id);

        Flash::success('User Detail updated successfully.');

        return redirect(route('userDetails.index'));
    }

    /**
     * Remove the specified User_detail from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $userDetail = $this->userDetailRepository->find($id);

        if (empty($userDetail)) {
            Flash::error('User Detail not found');

            return redirect(route('userDetails.index'));
        }

        $this->userDetailRepository->delete($id);

        Flash::success('User Detail deleted successfully.');

        return redirect(route('userDetails.index'));
    }
}
