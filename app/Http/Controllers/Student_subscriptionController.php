<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStudent_subscriptionRequest;
use App\Http\Requests\UpdateStudent_subscriptionRequest;
use App\Repositories\Student_subscriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class Student_subscriptionController extends AppBaseController
{
    /** @var  Student_subscriptionRepository */
    private $studentSubscriptionRepository;

    public function __construct(Student_subscriptionRepository $studentSubscriptionRepo)
    {
        $this->studentSubscriptionRepository = $studentSubscriptionRepo;
    }

    /**
     * Display a listing of the Student_subscription.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $studentSubscriptions = $this->studentSubscriptionRepository->all();

        return view('student_subscriptions.index')
            ->with('studentSubscriptions', $studentSubscriptions);
    }

    /**
     * Show the form for creating a new Student_subscription.
     *
     * @return Response
     */
    public function create()
    {
        return view('student_subscriptions.create');
    }

    /**
     * Store a newly created Student_subscription in storage.
     *
     * @param CreateStudent_subscriptionRequest $request
     *
     * @return Response
     */
    public function store(CreateStudent_subscriptionRequest $request)
    {
        $input = $request->all();

        $studentSubscription = $this->studentSubscriptionRepository->create($input);

        Flash::success('Student Subscription saved successfully.');

        return redirect(route('studentSubscriptions.index'));
    }

    /**
     * Display the specified Student_subscription.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $studentSubscription = $this->studentSubscriptionRepository->find($id);

        if (empty($studentSubscription)) {
            Flash::error('Student Subscription not found');

            return redirect(route('studentSubscriptions.index'));
        }

        return view('student_subscriptions.show')->with('studentSubscription', $studentSubscription);
    }

    /**
     * Show the form for editing the specified Student_subscription.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $studentSubscription = $this->studentSubscriptionRepository->find($id);

        if (empty($studentSubscription)) {
            Flash::error('Student Subscription not found');

            return redirect(route('studentSubscriptions.index'));
        }

        return view('student_subscriptions.edit')->with('studentSubscription', $studentSubscription);
    }

    /**
     * Update the specified Student_subscription in storage.
     *
     * @param int $id
     * @param UpdateStudent_subscriptionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStudent_subscriptionRequest $request)
    {
        $studentSubscription = $this->studentSubscriptionRepository->find($id);

        if (empty($studentSubscription)) {
            Flash::error('Student Subscription not found');

            return redirect(route('studentSubscriptions.index'));
        }

        $studentSubscription = $this->studentSubscriptionRepository->update($request->all(), $id);

        Flash::success('Student Subscription updated successfully.');

        return redirect(route('studentSubscriptions.index'));
    }

    /**
     * Remove the specified Student_subscription from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $studentSubscription = $this->studentSubscriptionRepository->find($id);

        if (empty($studentSubscription)) {
            Flash::error('Student Subscription not found');

            return redirect(route('studentSubscriptions.index'));
        }

        $this->studentSubscriptionRepository->delete($id);

        Flash::success('Student Subscription deleted successfully.');

        return redirect(route('studentSubscriptions.index'));
    }
}
