<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Student_subscription
 * @package App\Models
 * @version January 8, 2020, 5:21 pm UTC
 *
 * @property integer id_user
 * @property integer id_course
 * @property string subscription_date
 * @property integer status
 */
class Student_subscription extends Model
{
    use SoftDeletes;

    public $table = 'student_subscriptions';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_user',
        'id_course',
        'subscription_date',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_user' => 'integer',
        'id_course' => 'integer',
        'subscription_date' => 'date',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
