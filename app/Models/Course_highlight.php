<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Course_highlight
 * @package App\Models
 * @version January 10, 2020, 5:34 am UTC
 *
 * @property integer id_course
 * @property integer id_category
 * @property integer id_level
 * @property integer id_language
 * @property integer id_type
 * @property string beginning
 * @property number duration
 * @property integer number_of_lesson
 */
class Course_highlight extends Model
{
    use SoftDeletes;

    public $table = 'course_highlights';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_course',
        'id_category',
        'id_level',
        'id_language',
        'id_type',
        'beginning',
        'duration',
        'number_of_lesson'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_course' => 'integer',
        'id_category' => 'integer',
        'id_level' => 'integer',
        'id_language' => 'integer',
        'id_type' => 'integer',
        'beginning' => 'date',
        'duration' => 'double',
        'number_of_lesson' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_level' => 'required',
        'id_language' => 'required',
        'id_type' => 'required',
        'beginning' => 'required',
        'duration' => 'required'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function course()
    {
        return $this->hasOne(\App\Models\Course::class, 'id', 'Course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function levels()
    {
        return $this->belongsTo(\App\Models\Level::class, 'id', 'Level_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function types()
    {
        return $this->belongsTo(\App\Models\Type::class, 'id', 'Type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function categories()
    {
        return $this->belongsTo(\App\Models\Category::class, 'id', 'Category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function languages()
    {
        return $this->belongsTo(\App\Models\Language::class, 'id', 'Language_id');
    }
    
}
