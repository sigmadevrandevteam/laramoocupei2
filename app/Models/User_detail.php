<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User_detail
 * @package App\Models
 * @version January 8, 2020, 5:15 pm UTC
 *
 * @property integer id_user
 * @property string full_name
 * @property integer gender
 * @property string address
 * @property integer phone_number
 * @property string email
 * @property string birthday_date
 * @property string city
 * @property string nationality
 * @property string other
 */
class User_detail extends Model
{
    use SoftDeletes;

    public $table = 'user_details';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_user',
        'full_name',
        'gender',
        'address',
        'phone_number',
        'email',
        'birthday_date',
        'city',
        'nationality',
        'other'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_user' => 'integer',
        'full_name' => 'string',
        'gender' => 'integer',
        'address' => 'string',
        'phone_number' => 'integer',
        'email' => 'string',
        'birthday_date' => 'date',
        'city' => 'string',
        'nationality' => 'string',
        'other' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'full_name' => 'required',
        'gender' => 'required',
        'address' => 'required',
        'phone_number' => 'required',
        'email' => 'email',
        'birthday_date' => 'required'
    ];

    
}
