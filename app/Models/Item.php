<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Item
 * @package App\Models
 * @version January 13, 2020, 4:58 am UTC
 *
 * @property integer id_course
 * @property string file
 * @property integer file_type
 * @property string label
 */
class Item extends Model
{
    use SoftDeletes;

    public $table = 'items';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_course',
        'file',
        'file_type',
        'label'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_course' => 'integer',
        'file' => 'string',
        'file_type' => 'integer',
        'label' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
