<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Course
 * @package App\Models
 * @version January 8, 2020, 4:42 pm UTC
 *
 * @property string title
 * @property string photo
 * @property integer id_user
 * @property string about_the_course
 * @property string topic_include
 * @property integer view_counts
 */
class Course extends Model
{
    use SoftDeletes;

    public $table = 'courses';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'photo',
        'id_user',
        'about_the_course',
        'topic_include',
        'view_counts'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'photo' => 'string',
        'id_user' => 'integer',
        'about_the_course' => 'string',
        'topic_include' => 'string',
        'view_counts' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function course_highlight()
    {
        return $this->hasOne(\App\Models\Course_highlight::class, 'id', 'Course_id');
    }
}
